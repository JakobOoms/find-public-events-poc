﻿namespace Search.TargetAssembly
{
    // THis is a single line mycommand comments above attribute
    [PublicEvent]
    //single line comment above class declaration
    /*
     * Multi line awesome 
     */
    public class MyCommandSingleLine : IdspCommand
    {
        // Naajm commentz
        public int Name { get; set; }

        /// <summary>
        /// Prop2 commentz
        /// </summary>
        public string Prop2 { get; set; }
    }
}