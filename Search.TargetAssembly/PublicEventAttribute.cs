﻿using System;

namespace Search.TargetAssembly
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class PublicEventAttribute : Attribute
    {
    }


}
