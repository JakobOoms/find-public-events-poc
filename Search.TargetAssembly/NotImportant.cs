﻿namespace Search.TargetAssembly
{
    /// <summary>
    /// This class is not important because it is not marked with Attribute
    /// </summary>
    public class NotImportant : IdspCommand
    {
        /// <summary>
        /// No no no
        /// </summary>
        public int NoNo { get; set; }
    }
}