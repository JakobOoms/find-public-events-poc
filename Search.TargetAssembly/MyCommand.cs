﻿namespace Search.TargetAssembly
{
    /// <summary>
    /// My awesome command which is public, you fools!
    /// </summary>
    [PublicEvent]
    public class MyCommand : IdspCommand
    {
        /// <summary>
        /// What's...my...name
        /// </summary>
        public int Name { get; set; }

        /// <summary>
        /// Defining the aweomeness
        /// </summary>
        public string Shiiiit { get; set; }
    }
}