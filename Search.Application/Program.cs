﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using MarkdownLog;
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.MSBuild;

namespace Search.Application
{
    class Program
    {
        private class FindPublicEventsVisitor : SyntaxWalker
        {
            public ISet<ClassDeclarationSyntax> PublicEvents { get; }

            public FindPublicEventsVisitor() : base(SyntaxWalkerDepth.Token)
            {
                PublicEvents = new HashSet<ClassDeclarationSyntax>();
            }

            protected override void VisitToken(SyntaxToken token)
            {
                if (token.Parent is ClassDeclarationSyntax classDeclaration &&
                    classDeclaration.HasPublicEventAttribute())
                    PublicEvents.Add(classDeclaration);

                base.VisitToken(token);
            }
        }

        private static async Task<ISet<ClassDeclarationSyntax>> FindPublicEventsInSolution(string solutionFile)
        {
            var publicEventFinder = new FindPublicEventsVisitor();

            using (var workspace = MSBuildWorkspace.Create())
            {
                var solution = await workspace.OpenSolutionAsync(solutionFile);

                foreach (var project in solution.Projects)
                {
                    var compilation = await project.GetCompilationAsync(CancellationToken.None);

                    foreach (var syntaxTree in compilation.SyntaxTrees)
                    {
                        var root = syntaxTree.GetRoot();
                        publicEventFinder.Visit(root);
                    }
                }
            }

            return publicEventFinder.PublicEvents;
        }

        static async Task Main(string[] args)
        {
            var rootDirectory = args[0];
            MSBuildLocator.RegisterDefaults();

            var solutionFiles = Directory.GetFiles(rootDirectory, "*.sln", SearchOption.AllDirectories);
            var publicEvents = new HashSet<ClassDeclarationSyntax>();

            var building = Stopwatch.StartNew();
            
            foreach (var solutionFile in solutionFiles)
            {
                publicEvents.UnionWith(await FindPublicEventsInSolution(solutionFile));
            }

            Console.WriteLine("Building time seconds: " + building.Elapsed.TotalSeconds);

            foreach (var foundEvent in publicEvents)
            {
                var root = foundEvent.GetRoot();
                var trivias = root.DescendantTrivia().TakeWhile(x => x.SpanStart < foundEvent.Identifier.SpanStart)
                    .ToArray();
                var comments = trivias.ExtractComments();

                Console.WriteLine(new Header(foundEvent.Identifier.ToString()));
                Console.WriteLine(new Paragraph(comments));
                Console.WriteLine(MapMembersToTable(foundEvent.Members.OfType<PropertyDeclarationSyntax>()));
                Console.WriteLine(new HorizontalRule());
                Console.WriteLine();
            }

            Console.ReadLine();
        }

        private static Table MapMembersToTable(IEnumerable<PropertyDeclarationSyntax> properties)
        {
            return new Table
            {
                Columns = new[]
                {
                    new TableColumn {HeaderCell = new TableCell {Text = "Property name"}},
                    new TableColumn {HeaderCell = new TableCell {Text = "Property type"}},
                    new TableColumn {HeaderCell = new TableCell {Text = "Comments"}},
                },
                Rows = properties.Select(property => new TableRow
                {
                    Cells = new List<ITableCell>
                    {
                        new TableCell {Text = property.Identifier.ToString()},
                        new TableCell {Text = property.Type.ToString()},
                        new TableCell {Text = property.GetLeadingTrivia().ExtractComments()}
                    }
                })
            };
        }
    }

    public static class SyntaxTokenExtensions
    {
        public static string ExtractComments(this IEnumerable<SyntaxTrivia> triviaList)
        {
            var regEx = new Regex(@"<[^>]*>");
            var builder = new StringBuilder();

            foreach (var syntaxTrivia in triviaList)
            {
                if (syntaxTrivia.IsKind(SyntaxKind.SingleLineCommentTrivia))
                    builder.AppendLine(syntaxTrivia.Clean("//"));

                if (syntaxTrivia.IsKind(SyntaxKind.MultiLineCommentTrivia))
                    builder.AppendLine(syntaxTrivia.Clean("/*", "*/", "*"));

                if (syntaxTrivia.IsKind(SyntaxKind.SingleLineDocumentationCommentTrivia) ||
                    syntaxTrivia.IsKind(SyntaxKind.MultiLineDocumentationCommentTrivia))
                    builder.AppendLine(regEx.Replace(syntaxTrivia.Clean("///"), string.Empty));
            }

            return builder.ToString();
        }

        private static string Clean(this SyntaxTrivia input, params string[] remove)
        {
            var replacements = new List<string>(remove) { "\n", "\r", "\t", Environment.NewLine };
            var raw = input.ToString();
            raw = replacements.Aggregate(raw, (current, s) => current.Replace(s, string.Empty));
            return raw.Trim();
        }

        public static SyntaxNode GetRoot(this ClassDeclarationSyntax token)
        {
            SyntaxNode current = token;
            while (current.Parent != null)
            {
                current = current.Parent;
            }

            return current;
        }

        public static bool HasPublicEventAttribute(this ClassDeclarationSyntax classSyntax)
        {
            return classSyntax.AttributeLists.SelectMany(x => x.Attributes)
                .Any(attribute => attribute.Name.ToString() == "PublicEvent");
        }
    }
}
